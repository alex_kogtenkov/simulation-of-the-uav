package com.kogtenkov;

import com.jogamp.opengl.GL2;
import com.jogamp.common.nio.Buffers;
import java.io.*;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

public class GraphicModel {
    private GL2 gl;

    private int[] VBOVertices = new int[1];     // Vertex VBO Name
    private int[] VBOTexCoords = new int[1];    // Texture Coordinate VBO Name
    private int[] VBONormals = new int[1];
    private int[] EBO = new int[1];

    private FloatBuffer vertices;    // Vertex Data
    private IntBuffer indices;
    private int vertexCount;    // Vertex Count
    private int indexCount;

    private FloatBuffer texCoords;    // Texture Coordinates
    private IntBuffer texIndices;
    private int texCoordsCount;
    private int texIndicesCount;

    private FloatBuffer normals;
    private IntBuffer normalIndices;
    private int normalCount;
    private int normalIndicesCount;

    GraphicModel(GL2 gl, File world) {
        this.gl = gl;
        createModel(world);
    }

    private void load_model(File world) {
        FileReader fr;
        try {
            fr = new FileReader(world);
        }
        catch (FileNotFoundException e) {
            System.out.println("Model file does not exist.");
            return;
        }
        BufferedReader input = new BufferedReader(fr);
        String cur_string;
        try {
            cur_string = input.readLine();
        }
        catch (IOException e) {
            System.out.println("Exception while reading model.");
            return;
        }
        ArrayList<Float> tmp_vertices = new ArrayList<>();
        ArrayList<Integer> tmp_indices = new ArrayList<>();

        ArrayList<Float> tmp_texCoords = new ArrayList<>();
        ArrayList<Integer> tmp_texIndices = new ArrayList<>();

        ArrayList<Float> tmp_normals = new ArrayList<>();
        ArrayList<Integer> tmp_normalIndices = new ArrayList<>();
        vertexCount = 0;
        indexCount = 0;
        normalCount = 0;
        normalIndicesCount = 0;
        texCoordsCount = 0;
        texIndicesCount = 0;
        while (cur_string != null) {
            String[] substrings = cur_string.split(" ");
            if (substrings.length == 4 && substrings[0].equals("v")) {
                tmp_vertices.add(Float.valueOf(substrings[1]));
                tmp_vertices.add(Float.valueOf(substrings[2]));
                tmp_vertices.add(Float.valueOf(substrings[3]));
                vertexCount += 1;
            }
            else if (substrings.length == 4 && substrings[0].equals("vn")) {
                tmp_normals.add(Float.valueOf(substrings[1]));
                tmp_normals.add(Float.valueOf(substrings[2]));

                tmp_normals.add(Float.valueOf(substrings[3]));
                normalCount += 1;
            }
            else if (substrings.length == 3 && substrings[0].equals("vt")) {
                tmp_texCoords.add(Float.valueOf(substrings[1]));
                tmp_texCoords.add(Float.valueOf(substrings[2]));
                texCoordsCount += 1;
            }
            else if (substrings.length == 4 && substrings[0].equals("f")) {
                for (int i = 1; i < 4; i += 1) {
                    String[] tmp_s = substrings[i].split("/");
                    tmp_indices.add(Integer.valueOf(tmp_s[0]));
                    indexCount += 1;
                    if (tmp_s[1].length() > 0) {
                        tmp_texIndices.add(Integer.valueOf(tmp_s[1]));
                        texIndicesCount += 1;
                    }
                    if (tmp_s[2].length() > 0) {
                        tmp_normalIndices.add(Integer.valueOf(tmp_s[2]));
                        normalIndicesCount += 1;
                    }
                }
            }
            try {
                cur_string = input.readLine();
            }
            catch (IOException e) {
                System.out.println("Exception while reading model");
                return;
            }
        }

        vertices = Buffers.newDirectFloatBuffer(indexCount * 3);
        //vertices = BufferUtil.newFloatBuffer(indexCount * 3);
        indices = Buffers.newDirectIntBuffer(indexCount);
        //indices = BufferUtil.newIntBuffer(indexCount);
        texCoords = Buffers.newDirectFloatBuffer(texIndicesCount * 2);
        //texCoords = BufferUtil.newFloatBuffer(texIndicesCount * 2);
        texIndices = Buffers.newDirectIntBuffer(texIndicesCount);
        //texIndices = BufferUtil.newIntBuffer(texIndicesCount);
        normals = Buffers.newDirectFloatBuffer(normalIndicesCount * 3);
        //normals = BufferUtil.newFloatBuffer(normalIndicesCount * 3);
        normalIndices = Buffers.newDirectIntBuffer(normalIndicesCount);
        //normalIndices = BufferUtil.newIntBuffer(normalIndicesCount);
        /*for (int i = 0; i < vertexCount * 3; i += 1) {
            vertices.put(tmp_vertices.get(i));
        }*/
        for (int i = 0; i < indexCount; i += 1) {
            indices.put(tmp_indices.get(i) - 1);
        }
        /*for (int i = 0; i < normalCount; i += 1) {
            normals.put(tmp_normals.get(i));
        }*/
        for (int i = 0; i < normalIndicesCount; i += 1) {
            normalIndices.put(tmp_normalIndices.get(i) - 1);
        }
        /*for (int i = 0; i < texCoordsCount; i += 1) {
            texCoords.put(tmp_texCoords.get(i));
        }*/
        for (int i = 0; i < texIndicesCount; i += 1) {
            texIndices.put(tmp_texIndices.get(i) - 1);
        }


        for (int i = 0; i < texIndicesCount; i++) {
            texCoords.put(tmp_texCoords.get((tmp_texIndices.get(i) - 1) * 2));
            texCoords.put(tmp_texCoords.get((tmp_texIndices.get(i) - 1) * 2 + 1));
        }
        for (int i = 0; i < indexCount; i++) {
            vertices.put(tmp_vertices.get((tmp_indices.get(i) - 1) * 3));
            vertices.put(tmp_vertices.get((tmp_indices.get(i) - 1) * 3 + 1));
            vertices.put(tmp_vertices.get((tmp_indices.get(i) - 1) * 3 + 2));
        }
        for (int i = 0; i < normalIndicesCount; i++) {
            normals.put(tmp_normals.get((tmp_normalIndices.get(i) - 1) * 3));
            normals.put(tmp_normals.get((tmp_normalIndices.get(i) - 1) * 3 + 1));
            normals.put(tmp_normals.get((tmp_normalIndices.get(i) - 1) * 3 + 2));
        }
        normalCount = normalIndicesCount;
        texCoordsCount = texIndicesCount;
        vertexCount = indexCount;

        /*System.out.println(vertexCount);
        System.out.println(indexCount);
        System.out.println(normalCount);
        System.out.println(normalIndicesCount);
        System.out.println(texCoordsCount);
        System.out.println(texIndicesCount);*/
    }

    private void createModel(File model_file) {
        load_model(model_file);
        vertices.flip();
        indices.flip();
        texCoords.flip();
        normals.flip();
        texIndices.flip();
        normalIndices.flip();

        gl.glGenBuffers(1, VBOVertices, 0);  // Get A Valid Name
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, VBOVertices[0]);  // Bind The Buffer
        gl.glBufferData(gl.GL_ARRAY_BUFFER, vertexCount * 3 *
                Buffers.SIZEOF_FLOAT, vertices, gl.GL_STATIC_DRAW);

        // Generate And Bind The Texture Coordinate Buffer
        gl.glGenBuffers(1, VBOTexCoords, 0);  // Get A Valid Name
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, VBOTexCoords[0]); // Bind The Buffer
        gl.glBufferData(gl.GL_ARRAY_BUFFER, texCoordsCount * 2 *
                Buffers.SIZEOF_FLOAT, texCoords, gl.GL_STATIC_DRAW);

        gl.glGenBuffers(1, VBONormals, 0);  // Get A Valid Name
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, VBONormals[0]); // Bind The Buffer
        gl.glBufferData(gl.GL_ARRAY_BUFFER, normalCount * 3 *
                Buffers.SIZEOF_FLOAT, normals, gl.GL_STATIC_DRAW);

        texCoords = null;
        vertices = null;
        normals = null;
        texIndices = null;
        indices = null;
        normalIndices = null;

        /*gl.glGenBuffers(1, EBO, 0);  // Get A Valid Name
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, EBO[0]);  // Bind The Buffer
        gl.glBufferData(gl.GL_ELEMENT_ARRAY_BUFFER, indexCount *
                        BufferUtil.SIZEOF_INT, indices, gl.GL_STATIC_DRAW);*/
    }

    public void render(int texture) {
        gl.glEnableClientState(gl.GL_VERTEX_ARRAY);  // Enable Vertex Arrays
        gl.glEnableClientState(gl.GL_TEXTURE_COORD_ARRAY);
        gl.glEnableClientState(gl.GL_NORMAL_ARRAY);

        gl.glActiveTexture(GL2.GL_TEXTURE0);
        gl.glBindTexture(GL2.GL_TEXTURE_2D, texture);

        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, VBONormals[0]);
        gl.glNormalPointer(gl.GL_FLOAT, 0, 0);
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, VBOTexCoords[0]);
        gl.glTexCoordPointer(2, gl.GL_FLOAT, 0, 0);
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, VBOVertices[0]);
        gl.glVertexPointer(3, gl.GL_FLOAT, 0, 0);
        //gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, EBO[0]);
        //gl.glDrawElements(gl.GL_TRIANGLES, indexCount, gl.GL_UNSIGNED_INT, 0);
        gl.glDrawArrays(gl.GL_TRIANGLES, 0, vertexCount);

        gl.glDisableClientState(gl.GL_VERTEX_ARRAY);
        gl.glDisableClientState(gl.GL_TEXTURE_COORD_ARRAY);
        gl.glDisableClientState(gl.GL_NORMAL_ARRAY);
    }
}
