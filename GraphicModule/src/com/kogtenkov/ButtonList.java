package com.kogtenkov;

import com.jogamp.opengl.GL2;

import java.util.ArrayList;

public class ButtonList implements MenuListener {
    private ArrayList<MenuButton> buttons = new ArrayList<>();
    private boolean is_active = true;
    private boolean is_visible = true;
    private GL2 gl;
    private boolean[] keys;
    private InputModule input_module;
    private int x, y;
    private int size_x, size_y;
    private int x1, x2, y1, y2;
    private int shift = 0;
    private int button_size_x, button_size_y;
    private int vspace, hspace;

    ButtonList(int x, int y, int size_x, int size_y, GL2 gl, int button_size_x, int button_size_y, int vspace, int hspace, boolean[] keys, InputModule input_module) {
        this.gl = gl;
        this.keys = keys;
        this.input_module = input_module;
        this.x = x;
        this.y = y;
        this.size_x = size_x;
        this.size_y = size_y;
        this.button_size_x = button_size_x;
        this.button_size_y = button_size_y;
        this.vspace = vspace;
        this.hspace = hspace;
    }

    public void set_active(boolean is_active) {
        this.is_active = is_active;
    }

    public void set_visible(boolean is_visible) {
        this.is_visible = is_visible;
    }

    public void draw(int width, int height) {
        if (is_visible) {
            gl.glBindTexture(GL2.GL_TEXTURE_2D, 0);
            gl.glColor3f(0.5f, 0.5f, 0.5f);
            gl.glBegin(gl.GL_QUAD_STRIP);
            gl.glVertex2f(x1, y1);
            gl.glVertex2f(x2, y1);
            gl.glVertex2f(x1, y2);
            gl.glVertex2f(x2, y2);
            gl.glEnd();
            gl.glColor3f(1, 1, 1);
            for (int i = shift; i < buttons.size(); i++) {
                buttons.get(i).draw(width, height);
            }
        }
    }

    public void addButton(String title, String event_name, int text_x, int text_y, int font_size) {
        buttons.add(new MenuButton(x + hspace, 0, button_size_x, button_size_y, title, gl, keys, event_name, text_x, text_y, font_size));
    }

    public void deleteButton(String event_name) {
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons.get(i).get_event_name().equals(event_name)) {
                buttons.remove(i);
            }
        }
    }

    public void addListener(MenuListener menuListener, String event_name) {
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons.get(i).get_event_name().equals(event_name)) {
                buttons.get(i).addListener(menuListener);
            }
        }
    }

    public boolean isCursorOnButton(int mouse_x, int mouse_y) {
        return (mouse_x >= x1 && mouse_x <= x2 && mouse_y >= y1 && mouse_y <= y2);
    }

    public void work(int width, int height) {
        if (is_active) {
            if (x >= 0) {
                x1 = x;
                x2 = x + size_x;
            } else {
                x1 = width + x;
                x2 = x1 + size_x;
            }
            if (y >= 0) {
                y1 = y;
                y2 = y + size_y;
            } else {
                y1 = height + y;
                y2 = y1 + size_y;
            }
            if (isCursorOnButton(input_module.cur_mouse_x, height - input_module.cur_mouse_y)) {
                if (input_module.mouse_rotation < 0) {
                    shift += 1;
                } else if (input_module.mouse_rotation > 0){
                    shift = Math.max(0, shift - 1);
                }
                input_module.mouse_rotation = 0;
            }
            int num_visible_buttons = (size_y + vspace) / (vspace + button_size_y);
            num_visible_buttons = Math.min(num_visible_buttons, buttons.size());
            shift = Math.min(shift, buttons.size() - num_visible_buttons);
            for (int i = 0; i < buttons.size(); i++) {
                if (i < shift || i >= shift + num_visible_buttons) {
                    buttons.get(i).is_active = false;
                    buttons.get(i).is_visible = false;
                }
                else {
                    buttons.get(i).is_active = true;
                    buttons.get(i).is_visible = true;
                }
            }
            for (int i = shift; i < shift + num_visible_buttons; i++) {
                buttons.get(i).setY(y + size_y - (vspace + button_size_y) * (i - shift + 1));
                boolean check_status = buttons.get(i).check_click(input_module.cur_mouse_x, input_module.cur_mouse_y, width, height);
                if (check_status) {
                    keys[0] = false;
                    return;
                }
            }
        }
    }

    @Override
    public void menuEventReceiver(MenuEvent e) {
        if (e.name.equals("show_list")) {
            is_active = true;
            is_visible = true;
        }
        else if (e.name.equals("hide_list")) {
            is_visible = false;
            is_active = false;
        }
    }
}
