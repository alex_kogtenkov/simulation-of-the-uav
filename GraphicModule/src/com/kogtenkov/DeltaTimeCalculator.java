package com.kogtenkov;

import java.util.Date;

public class DeltaTimeCalculator {
    private long prev_time = -1;
    private long cur_time = -1;
    private double[] deltas;
    private int ind_deltas = 0;
    private double sum_delta_time = 0;
    private double num_measurements = 0;

    public DeltaTimeCalculator() {
        Date date = new Date();
        cur_time = date.getTime();
        prev_time = cur_time;
        sum_delta_time = 0;
        deltas = new double[] {0, 0, 0, 0, 0, 0, 0, 0};
        ind_deltas = 0;
        num_measurements = 0;
    }

    public DeltaTimeCalculator(int average_sequence_len) {
        Date date = new Date();
        cur_time = date.getTime();
        prev_time = cur_time;
        sum_delta_time = 0;
        deltas = new double[average_sequence_len];
        ind_deltas = 0;
        num_measurements = 0;
    }

    public void reset(int average_sequence_len) {
        Date date = new Date();
        cur_time = date.getTime();
        prev_time = cur_time;
        sum_delta_time = 0;
        ind_deltas = 0;
        deltas = new double[average_sequence_len];
        num_measurements = 0;
    }

    public void update() {
        prev_time = cur_time;
        cur_time = (new Date()).getTime();
        sum_delta_time -= deltas[ind_deltas];
        deltas[ind_deltas] = (double)(cur_time - prev_time) / 1000;
        sum_delta_time += deltas[ind_deltas];
        ind_deltas = (ind_deltas + 1) % deltas.length;
        num_measurements = Math.min(num_measurements + 1, deltas.length);
    }

    public double getDelta() {
        return sum_delta_time / num_measurements;
    }

    public long getCurTime() {
        return cur_time;
    }
}
