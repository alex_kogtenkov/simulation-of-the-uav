package com.kogtenkov;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import java.io.File;
import java.io.IOException;

public class World {
    private int[] textures;
    private GL2 gl;
    private GraphicModel model;

    public World(File world, File texture, GL2 gl) {
        //Load world from filename
        this.gl = gl;
        textures = new int[1];
        LoadTexture(texture, 0);
        this.gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT );
        this.gl.glTexParameterf(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT );
        model = new GraphicModel(gl, world);
    }

    private void LoadTexture(File image, int ind) {
        try {
            Texture t = TextureIO.newTexture(image, true);
            textures[ind] = t.getTextureObject(gl);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void Draw() {
        model.render(textures[0]);
    }
}
