package com.kogtenkov;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.glu.GLU;
import static java.lang.Math.*;

public class FreeCamera extends Camera {
    private double vx = 0, vy = 0, vz = 1;
    private double rotSpeed = PI / 2;
    private Followable object;
    private boolean look_at_object;

    public FreeCamera(GLWindow glwindow, InputModule input_module, double x, double y, double z,
                      String name, double hrot, double vrot, boolean[] keys, DeltaTimeCalculator deltaTimeCalculator) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.name = name;
        this.hrot = hrot;
        this.vrot = vrot;
        this.vx = cos(hrot) * cos(vrot);
        this.vy = sin(vrot);
        this.vz = sin(hrot) * cos(vrot);

        this.keys = keys;
        this.glwindow = glwindow;
        this.input_module = input_module;
        this.look_at_object = false;

        this.deltaTimeCalculator = deltaTimeCalculator;
    }

    public void work(GLU glu) {
        double dv = speed * deltaTimeCalculator.getDelta();
        double delta_time = deltaTimeCalculator.getDelta();
        if (keys[2]) {
            glwindow.setPointerVisible(false);
            glwindow.confinePointer(true);
            int mouse_dx = input_module.cur_mouse_x - input_module.prev_mouse_x;
            int mouse_dy = input_module.cur_mouse_y - input_module.prev_mouse_y;
            hrot += mouse_dx * mouseRotSpeed * delta_time;
            vrot -= mouse_dy * mouseRotSpeed * delta_time;
            if (keys['Q']) hrot -= rotSpeed * delta_time;
            if (keys['E']) hrot += rotSpeed * delta_time;
            hrot %= (2 * PI);
            if (vrot <= -PI / 2 + 0.005) vrot = -PI / 2 + 0.005;
            else if (vrot >= PI / 2 - 0.005) vrot = PI / 2 - 0.005;

            if (keys['W']) {
                x += vx * dv; y += vy * dv; z += vz * dv;
            }
            if (keys['S']) {
                x -= vx * dv; y -= vy * dv; z -= vz * dv;
            }
            if (keys['A']) {
                double x_full = vx / sqrt(1 - vy * vy);
                double z_full = vz / sqrt(1 - vy * vy);
                x += z_full * dv;
                z -= x_full * dv;
            }
            if (keys['D']) {
                double x_full = vx / sqrt(1 - vy * vy);
                double z_full = vz / sqrt(1 - vy * vy);
                x -= z_full * dv;
                z += x_full * dv;
            }
            if (keys[32]) {     //SPACE
                y += dv;
            }
            if (keys[15]) {     //SHIFT
                y -= dv;
            }
            vx = cos(hrot) * cos(vrot);
            vy = sin(vrot);
            vz = sin(hrot) * cos(vrot);
            glwindow.warpPointer(input_module.prev_mouse_x, input_module.prev_mouse_y);
            input_module.cur_mouse_x = input_module.prev_mouse_x;
            input_module.cur_mouse_y = input_module.prev_mouse_y;
        }
        else {
            glwindow.setPointerVisible(true);
            glwindow.confinePointer(false);
        }
        if (!look_at_object) {
            glu.gluLookAt(x, y, z, x + vx, y + vy, z + vz, 0, 1, 0);
        }
        else {
            glu.gluLookAt(x, y, z, object.getX(), object.getY(), object.getZ(), 0, 1, 0);
        }
    }

    public void attach(Followable object) {
        this.look_at_object = true;
        this.object = object;
    }

    public void disattach() {
        this.look_at_object = false;
        this.object = null;
    }

    public Followable getObject() {
        return object;
    }

    @Override
    public String getType() {
        return "Free";
    }
}
