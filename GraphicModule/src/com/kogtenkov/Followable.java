package com.kogtenkov;

public interface Followable {
    double getX();
    double getY();
    double getZ();
    double getDirectionX();
    double getDirectionY();
    double getDirectionZ();
}
