package com.kogtenkov;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.math.Quaternion;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import com.yuriy.client.Context;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

class Point3D {
    private float x, y, z;
    Point3D(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    Point3D(double x, double y, double z) {
        this.x = (float) x;
        this.y = (float) y;
        this.z = (float) z;
    }
    public float getX() {
        return x;
    }
    public float getY() {
        return y;
    }
    public float getZ() {
        return z;
    }
}

public class Quadcopter implements Followable {
    private int id;

    private float x = 0, y = 0, z = 0;
    private float xrot = 0, yrot = 0, zrot = 0;
    private Quaternion q;
    private ArrayList<AtomicInteger> atomic_quaternion;
    private float[] rotation_matrix;
    private float[] propeller_rot_matrix;
    private GL2 gl;
    private int[] textures;

    private DeltaTimeCalculator deltaTimeCalculator;

    private GraphicModel model;
    private GraphicModel propeller_left;
    private GraphicModel propeller_right;

    private Path dataDir;

    private Point3D[] prop_pos = new Point3D[] {
            new Point3D(0.704, 0.19, 0.704),
            new Point3D(0.704, 0.19, -0.704),
            new Point3D(-0.704, 0.19, -0.704),
            new Point3D(-0.704, 0.19, 0.704)};
    private Point3D light_pos = new Point3D(0.0, -0.5, 0.0);
    private float[] light_color = new float[] {1.0f, 0.1f, 0.1f};

    private float[] prop_speed = new float[] {0.0f, 0.0f, 0.0f, 0.0f};
    private float[] prop_rotation = new float[] {0f, 0f, 0f, 0f};
    private float[] speed = new float[] {0f, 0f, 0f};

    public Quadcopter(Path dataDir, File quadcopter_info,
                      GL2 gl, DeltaTimeCalculator deltaTimeCalculator, Context api) {
        this.dataDir = dataDir;
        this.q = new Quaternion(0, 1, 0, 0);
        this.rotation_matrix = new float[16];
        this.propeller_rot_matrix = new float[16];
        this.gl = gl;
        loadQuadcopterStates(quadcopter_info);

        this.deltaTimeCalculator = deltaTimeCalculator;

        atomic_quaternion = new ArrayList<>();
        try {
            ArrayList<AtomicInteger> atomic_variables = api.allocate("quadcopter_states", 3 + 3 + 3 + 1 + 3 + 4, AtomicInteger::new);
            for (Object o : atomic_variables) {
                atomic_quaternion.add((AtomicInteger)o);
            }
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    private void loadQuadcopterStates(File infoFile) {
        Scanner scanner;
        try {
            scanner = new Scanner(infoFile).useLocale(Locale.US);
        } catch (Exception e) {
            System.out.println("Cannot read quadcopter states");
            return;
        }
        float x, y, z;
        for (int i = 0; i < 4; i++) {
            x = scanner.nextFloat();
            y = scanner.nextFloat();
            z = scanner.nextFloat();
            prop_pos[i] = new Point3D(x, y, z);
        }
        x = scanner.nextFloat();
        y = scanner.nextFloat();
        z = scanner.nextFloat();
        light_pos = new Point3D(x, y, z);
        light_color[0] = scanner.nextFloat();
        light_color[1] = scanner.nextFloat();
        light_color[2] = scanner.nextFloat();
        scanner.nextLine();
        File model_file = this.dataDir.resolve(scanner.nextLine().trim()).toFile();
        File model_prop_left = this.dataDir.resolve(scanner.nextLine().trim()).toFile();
        File model_prop_right = this.dataDir.resolve(scanner.nextLine().trim()).toFile();
        this.model = new GraphicModel(gl, model_file);
        this.propeller_left = new GraphicModel(gl, model_prop_left);
        this.propeller_right = new GraphicModel(gl, model_prop_right);
        this.textures = new int[2];
        String texture_filename = scanner.nextLine().trim();
        String texture_prop = scanner.nextLine().trim();
        scanner.close();
        loadTexture(texture_filename, 0);
        loadTexture(texture_prop, 1);
    }

    private void loadTexture(String s, int ind) {
        try {
            File im = new File("data/textures/" + s);
            Texture t = TextureIO.newTexture(im, true);
            textures[ind] = t.getTextureObject(gl);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void update() {
        float dt = (float)deltaTimeCalculator.getDelta();
        q.setW(Float.intBitsToFloat(atomic_quaternion.get(9).get()));
        q.setX(Float.intBitsToFloat(atomic_quaternion.get(10).get()));
        q.setY(Float.intBitsToFloat(atomic_quaternion.get(12).get()));
        q.setZ(Float.intBitsToFloat(atomic_quaternion.get(11).get()));
        x = Float.intBitsToFloat(atomic_quaternion.get(0).get());
        y = Float.intBitsToFloat(atomic_quaternion.get(2).get());
        z = Float.intBitsToFloat(atomic_quaternion.get(1).get());
        speed[0] = Float.intBitsToFloat(atomic_quaternion.get(0).get());
        speed[1] = Float.intBitsToFloat(atomic_quaternion.get(2).get());
        speed[2] = Float.intBitsToFloat(atomic_quaternion.get(1).get());
        prop_speed[0] = Float.intBitsToFloat(atomic_quaternion.get(13).get());
        prop_speed[1] = Float.intBitsToFloat(atomic_quaternion.get(14).get());
        prop_speed[2] = Float.intBitsToFloat(atomic_quaternion.get(15).get());
        prop_speed[3] = Float.intBitsToFloat(atomic_quaternion.get(16).get());
        for (int i = 0; i < prop_rotation.length; i++) {
            prop_rotation[i] += dt * prop_speed[i] * 10000.0;
        }
        rotation_matrix = q.toMatrix(rotation_matrix, 0);
    }

    public void draw() {
        //float dt = (float)deltaTimeCalculator.getDelta();
        /*x += dt * speed[0];
        y += dt * speed[1];
        z += dt * speed[2];
        */

        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, new float[] {0.0f, 0.0f, 0.0f, 0.0f}, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, new float[] {light_color[0], light_color[1], light_color[2], 0.0f}, 0);
        gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, new float[] {x + (float)light_pos.getX(), y + (float)light_pos.getY(), z + (float)light_pos.getZ(), 1.0f}, 0);
        gl.glLightf(GL2.GL_LIGHT1, GL2.GL_QUADRATIC_ATTENUATION, 0.03f);

        float[] dir_axis = new float[3];
        dir_axis[0] = dir_axis[2] = 0;
        dir_axis[1] = 1f;
        //q.setFromAngleNormalAxis(xrot, dir_axis);

        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glMultMatrixf(rotation_matrix, 0);
        model.render(textures[0]);
        gl.glPopMatrix();

        dir_axis = new float[3];
        dir_axis[0] = dir_axis[2] = 0;
        dir_axis[1] = 1;

        for (int i = 0; i < prop_rotation.length; i++) {
            q.setFromAngleNormalAxis(prop_rotation[i], dir_axis);
            propeller_rot_matrix = q.toMatrix(propeller_rot_matrix, 0);

            gl.glPushMatrix();
            gl.glTranslatef(x, y, z);
            gl.glMultMatrixf(rotation_matrix, 0);
            gl.glTranslated(prop_pos[i].getX(), prop_pos[i].getY(), prop_pos[i].getZ());
            gl.glMultMatrixf(propeller_rot_matrix, 0);
            if (i % 2 == 0) {
                propeller_left.render(textures[1]);
            }
            else propeller_right.render(textures[1]);
            gl.glPopMatrix();
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }

    @Override
    public double getDirectionX() {
        //return cos(hrot) * cos(vrot);
        return 0;
    }

    @Override
    public double getDirectionY() {
        //return sin(vrot);
        return 0;
    }

    @Override
    public double getDirectionZ() {
        //return sin(hrot) * cos(vrot);
        return 0;
    }
}
