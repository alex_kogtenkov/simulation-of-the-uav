#include <iostream>
#include <map>
#include <vector>
#include <cstdio>
#include <string>
#include <fstream>

struct Vertex
{
    double x, y, z;

    Vertex(double a, double b, double c)
    {
        x = a;
        y = b;
        z = c;
    }

    const bool operator==(const Vertex &other)
    {
        return (x == other.x && y == other.y && z == other.z);
    }

    const bool operator!=(const Vertex &other)
    {
        return (x != other.x || y != other.y || z != other.z);
    }
};

struct VT
{
    double x, y;

    VT(double a, double b)
    {
        x = a;
        y = b;
    }

    const bool operator==(const VT &other)
    {
        return (x == other.x && y == other.y);
    }

    const bool operator!=(const VT &other)
    {
        return (x != other.x || y != other.y);
    }
};

int main()
{
    std::string model_file, output_file;
    double tex_scale_x = 1, tex_scale_y = 1;
    std::cout << "File with origin model:" << std::endl;
    std::cin >> model_file;
    std::cout << "Output file:" << std::endl;
    std::cin >> output_file;
    std::cout << "Texture scale x:" << std::endl;
    std::cin >> tex_scale_x;
    std::cout << "Texture scale y:" << std::endl;
    std::cin >> tex_scale_y;
    std::ifstream fin(model_file.c_str());
    if (!fin.is_open())
    {
        std::cout << "Cannot open file \"" << model_file << "\"" << std::endl;
        return 0;
    }
    std::ofstream fout(output_file.c_str());
    if (!fout.is_open())
    {
        std::cout << "Cannot open file \"" << model_file << "\"" << std::endl;
        return 0;
    }
    std::string cur_s = "";
    double valx, valy, valz;
    int indv, indvt, indvn;
    std::vector<Vertex> vertices;
    std::vector<VT> tex_coords;
    std::vector<Vertex> normals;
    std::vector<int> vertex_ind2ind;
    std::vector<int> vt_ind2ind;
    std::vector<int> vn_ind2ind;
    bool data_written = false;
    int num_vt = 0;
    int num_vn = 0;
    while (fin >> cur_s)
    {
        if (cur_s == "v")
        {
            fin >> valx >> valy >> valz;
            Vertex tmp(valx, valy, valz);
            int pos = 0;
            while (pos < vertices.size() && vertices[pos] != tmp) pos++;
            if (pos == vertices.size())
            {
                vertices.push_back(tmp);
            }
            vertex_ind2ind.push_back(pos);
        }
        else if (cur_s == "vt")
        {
            fin >> valx >> valy;
            VT tmp(valx * tex_scale_x, valy * tex_scale_y);
            int pos = 0;
            while (pos < tex_coords.size() && tex_coords[pos] != tmp) pos++;
            if (pos == tex_coords.size())
            {
                tex_coords.push_back(tmp);
            }
            vt_ind2ind.push_back(pos);
        }
        else if (cur_s == "vn")
        {
            fin >> valx >> valy >> valz;
            Vertex tmp(valx, valy, valz);
            int pos = 0;
            while (pos < normals.size() && normals[pos] != tmp) pos++;
            if (pos == normals.size())
            {
                normals.push_back(tmp);
            }
            vn_ind2ind.push_back(pos);
        }
        else if (cur_s == "f")
        {
            if (!data_written)
            {
                data_written = true;
                fout.setf(std::ios_base::fixed, std::ios_base::floatfield);
                fout.precision(6);
                for (int i = 0; i < vertices.size(); i++)
                {
                    fout << "v " << vertices[i].x << " " << vertices[i].y << " " << vertices[i].z << std::endl;
                }
                for (int i = 0; i < tex_coords.size(); i++)
                {
                    fout << "vt " << tex_coords[i].x << " " << tex_coords[i].y << std::endl;
                }
                fout.precision(4);
                for (int i = 0; i < normals.size(); i++)
                {
                    fout << "vn " << normals[i].x << " " << normals[i].y << " " << normals[i].z << std::endl;
                }
                fout << "s off" << std::endl;
            }
            fout << "f";
            for (int i = 0; i < 3; i++)
            {
                fin >> cur_s;
                sscanf(cur_s.c_str(), "%d/%d/%d", &indv, &indvt, &indvn);
                indv = vertex_ind2ind[indv - 1];
                indvt = vt_ind2ind[indvt - 1];
                indvn = vn_ind2ind[indvn - 1];
                fout << " " << indv + 1 << "/" << indvt + 1 << "/" << indvn + 1;
            }
            fout << std::endl;
        }
    }
    fin.close();
    return 0;
}
