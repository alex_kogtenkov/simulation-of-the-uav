//infantry combat vehicle
public class Ifv extends MilitaryUnit implements CrewCommands {
    private int capacity=0;
    private int troops=0;

    public Ifv(int health, float resistance, int crew, int capacity, int troops){
        super(health, resistance, crew, true);
        this.capacity = capacity;
        this.troops = troops;
    }

    public int getCapacity(){
        return capacity;
    }

    public int getTroops(){
        return troops;
    }

    public void setTroops(int troops){
        this.troops = troops;
    }

    public void shot(){
        System.out.println("BO-O-O-OM!!");
    }

    @Override
    public int addTroops(int n_troops) {
        troops += n_troops;
        int extra_troops = troops - capacity;
        if (extra_troops > 0){
            troops = capacity;
            return extra_troops;
        }
        return 0;
    }

    @Override
    public int removeTroops(int n_troops) {
        troops -= n_troops;
        if (troops < 0){
            int extra_troops = -troops;
            troops = 0;
            return n_troops - extra_troops;
        }
        return n_troops;
    }

    @Override
    public int damage(int damage) {
        health -= (int)((float)damage * (1 - 0.5 * resistance));
        if (health <= 0) {
            health = 0;
            alive = false;
        }
        return health;
    }

    @Override
    public void destroy() {
        health = 0;
        alive = false;
    }
}
