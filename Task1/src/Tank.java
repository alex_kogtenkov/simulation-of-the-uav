public class Tank extends MilitaryUnit {
    private int gun_type = 0;

    public Tank(int health, int resistance, int crew, int gun_type){
        super(health, resistance, crew, true);
        this.gun_type = gun_type;
    }

    public void setGun_type(int gun_type){
        this.gun_type = gun_type;
    }

    public int getGun_type(){
        return gun_type;
    }

    @Override
    public int damage(int damage) {
        health -= (int)((float)damage * (1 - resistance));
        if (health <= 0) {
            health = 0;
            alive = false;
        }
        return health;
    }

    @Override
    public void destroy() {
        health = 0;
        alive = false;
    }

}
