import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class ThreadAdder implements Runnable {
    ArrayList<Container> container_list;
    Semaphore semaphore;

    public ThreadAdder(ArrayList<Container> container_list, Semaphore semaphore) {
        this.container_list = container_list;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        int iter = 0;
        while (true) {
            try {
                semaphore.acquire(1);
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
                return;
            }
            synchronized (container_list) {
                container_list.add(new Container(Integer.toString(iter)));
            }
            System.out.println("ADDED " + iter);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
                return;
            }
            iter += 1;
        }
    }
}
